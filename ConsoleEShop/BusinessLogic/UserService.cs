﻿using ConsoleEShop.DataAccess;
using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.BusinessLogic
{
    public class UserService : IUserService
    {
        readonly IUserRepository userRepository;
        public UserService()
        {
            userRepository = new UserRepository();
        }

        public UserService(IUserRepository userService)
        {
            userRepository = userService;
        }

        public IList<User> GetAllUsers()
        {
            return userRepository.FindAll();
        }

        public List<User> GetUsersByName(string key)
        {
            return userRepository.FindByName(key);
        }

        public User AddNewUser(string name, string email, string password)
        {
            User user = new() { Name = name, Login = email, Password = password, IsAdmin = false };
            var existingUser = userRepository.GetUserByEmail(email);
            if (existingUser == null) { 
                userRepository.Insert(user);
                return user;
            }
            return null;
        }

        public User LogIn(string login, string password)
        {
            var user = userRepository.GetUserByEmail(login);
            if (user?.Password == password)
            {
                return user;
            }
            return null;
        }

        public User ChangeAccountInfo(string login, Action<User> action)
        {
            var user = userRepository.GetUserByEmail(login);
            if (user != null)
            {
                action.Invoke(user);
            }
            return user;
        }
    }
}

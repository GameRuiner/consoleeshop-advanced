﻿using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.DataAccess
{
    class OrderRepository : IOrderRepository
    {
        readonly List<Order> orders = new();

        public OrderRepository()
        {
            orders = new List<Order>
            {
                new Order { Id = 1, Customer = new User(), DateTime = DateTime.Now, Status = OrderStatus.New,  OrderItems = new List<OrderItems>()},
            };
        }

        public int GetNewID()
        {
            return orders.Max(o => o.Id) + 1;
        }

        public void Insert(Order order)
        {
            orders.Add(order);
        }

        public Order FindById(int id)
        {
            return (from order in orders
                    where order.Id == id
                    select order).FirstOrDefault();
        }

        public IEnumerable<Order> FindByUser(User user)
        {
            return from order in orders
                   where order.Customer.Login == user.Login
                   select order;
        }
    }
}

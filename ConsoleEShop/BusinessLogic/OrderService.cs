﻿using ConsoleEShop.DataAccess;
using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.BusinessLogic
{
    public class OrderService: IOrderService
    {
        readonly IOrderRepository orderRepository;
        public OrderService()
        {
            orderRepository = new OrderRepository();
        }

        public OrderService(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        public Order CreateNewOrder(int ItemId, int quantitity, User user, IProductService productService)
        {
            Product product = productService.GetProductById(ItemId);
            if (product == null) return null;
            OrderItems orderItems = new() { Product = product, Quantitity = quantitity };
            int newOrderId = orderRepository.GetNewID();
            Order order = new() { Id = newOrderId, Customer = user, DateTime = DateTime.Now, OrderItems = new List<OrderItems>() { orderItems }, Status = OrderStatus.New };
            orderRepository.Insert(order);
            return order;
        }

        public Order GetOrderById(int id)
        {
            return orderRepository.FindById(id);
        }

        public bool CancelOrder(int orderId, User user)
        {
            Order order = GetOrderById(orderId);
            if (order != null && order.Customer.Login == user.Login && order.Status != OrderStatus.Received)
            {
                order.Status = OrderStatus.CanceledByUser;
                return true;
            }
            return false;
        }

        public bool RecieveOrder(int orderId, User user)
        {
            Order order = GetOrderById(orderId);
            if (order != null && order.Customer.Login == user.Login)
            {
                order.Status = OrderStatus.Received;
                return true;
            }
            return false;
        }

        public IEnumerable<Order> GetUserOrders(User user)
        {
            var orders = orderRepository.FindByUser(user);
            return orders;
        }

        public Order ChangeOrderStatus(int orderId, Action<Order> action)
        {
            var order = orderRepository.FindById(orderId);
            if (order != null)
            {
                action.Invoke(order);
            }
            return order;
        }
    }
}

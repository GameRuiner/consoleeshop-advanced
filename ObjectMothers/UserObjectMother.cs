﻿using ConsoleEShop.Model;
using System;

namespace ObjectMothers
{
    public class UserObjectMother
    {
        public static User CreateRegisteredUserWithNameMarkoPolo()
        {
            User user = new() { Name = "Marko Polo", Login = "markopolo@epam.com", Password = "123"};
            return user;
        }

        public static User CreateRegisteredUserWithNameMarkoGolovko()
        {
            User user = new() { Name = "Marko Golovko", Login = "markogolovko@epam.com", Password = "1243" };
            return user;
        }
    }
}

﻿using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.DataAccess
{
    class UserRepository : IUserRepository
    {
        readonly List<User> users = new();

        public UserRepository()
        {
            users = new List<User>
            {
                new User() { Name = "Marko", Login = "marko@epam.com", Password = "123", IsAdmin = false},
                new User() { Name = "Dima", Login = "dima", Password = "123", IsAdmin = true},
            };
        }

        public List<User> FindAll()
        {
            return users;
        }

        public void Insert(User user)
        {
            users.Add(user);
        }

        public List<User> FindByName(string key)
        {
            IEnumerable<User> registeredUsers = users.Cast<User>();
            return (from user in registeredUsers
                    where user.Name.ToLower().Contains(key.Trim().ToLower())
                    select user).ToList();
        }

        public User GetUserByEmail (string key)
        {
            IEnumerable<User> registeredUsers = users.Cast<User>();
            return (from user in registeredUsers
                    where user.Login == key.Trim()
                    select user).FirstOrDefault();
        }
    }
}

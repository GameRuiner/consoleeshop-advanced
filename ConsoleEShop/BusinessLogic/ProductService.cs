﻿using ConsoleEShop.DataAccess;
using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.BusinessLogic
{
    public class ProductService : IProductService
    {
        readonly IProductRepository productRepository;
        public ProductService()
        {
            productRepository = new ProductRepository();
        }

        public ProductService(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public IList<Product> GetAllProducts()
        {
            return productRepository.FindAll();
        }

        public IList<Product> GetProductsByName(string key)
        {
            return productRepository.FindByName(key);
        }

        public Product GetProductById(int id)
        {
            return productRepository.FindById(id);
        }

        public Product AddNewProduct(string name, double price)
        {
            int newId = productRepository.GetNewID();
            Product product = new() { Name = name, Price = price, Id = newId };
            productRepository.Insert(product);
            return product;
        }

        public Product ChangeProductInfo(int id, Action<Product> action)
        {
            var product = productRepository.FindById(id);
            if (product != null)
            {
                action.Invoke(product);
            }
            return product;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.Model
{
    public class OrderItems
    {
        public Product Product { get; set; }
        public int Quantitity { get; set; }
    }
}

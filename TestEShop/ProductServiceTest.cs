﻿using ConsoleEShop.BusinessLogic;
using ConsoleEShop.DataAccess;
using ConsoleEShop.Model;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEShop
{
    class ProductServiceTest
    {
        [Test]
        public void CheckGetAllProducts()
        {
            Mock<IProductRepository> repositoryMock = new();
            ProductService ps = new(repositoryMock.Object);

            IList<Product> users = ps.GetAllProducts();

            repositoryMock.Verify(k => k.FindAll(), Times.Once());
        }

        [Test]
        public void CheckGetProductsByName()
        {
            Mock<IProductRepository> repositoryMock = new();
            ProductService ps = new(repositoryMock.Object);

            string key = "pop";
            IList<Product> users = ps.GetProductsByName(key);

            repositoryMock.Verify(k => k.FindByName(key), Times.Once());
        }

        [Test]
        public void CheckGetProductById()
        {
            Mock<IProductRepository> repositoryMock = new();
            ProductService ps = new(repositoryMock.Object);

            int id = 1;
            ps.GetProductById(id);

            repositoryMock.Verify(k => k.FindById(id), Times.Once());
        }

        [Test]
        public void CheckAddNewProduct()
        {
            Mock<IProductRepository> repositoryMock = new();
            ProductService ps = new(repositoryMock.Object);

            double price = 20000;
            string name = "PC";
            Product product = ps.AddNewProduct(name, price);

            repositoryMock.Verify(k => k.Insert(product), Times.Once());
        }

        [Test]
        public void CheckChangeProductInfo()
        {
            Mock<IProductRepository> repositoryMock = new();
            ProductService ps = new(repositoryMock.Object);

            int id = 1;
            ps.ChangeProductInfo(id, x => x.Name = "New Model");

            repositoryMock.Verify(k => k.FindById(id), Times.Once());
        }
    }
}

﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleEShop.DataAccess;
using ConsoleEShop.BusinessLogic;
using ConsoleEShop.Model;
using ObjectMothers;

namespace TestEShop
{
    class UserServiceTest
    {
        [Test]
        public void CheckGetAllUsers()
        {
            Mock<IUserRepository> repositoryMock = new();
            UserService us = new(repositoryMock.Object);

            IList<User> users = us.GetAllUsers();

            repositoryMock.Verify(k => k.FindAll(), Times.Once());
        }

        [Test]
        public void CheckFindByName()
        {
            Mock<IUserRepository> repositoryMock = new();
            UserService us = new(repositoryMock.Object);
            string name = "Marko";

            List<User> users = us.GetUsersByName(name);

            repositoryMock.Verify(k => k.FindByName(name), Times.Once());
        }

        [Test]
        public void CheckAddNewUser()
        {
            Mock<IUserRepository> repositoryMock = new();
            UserService us = new(repositoryMock.Object);
            User user = UserObjectMother.CreateRegisteredUserWithNameMarkoGolovko();

            user = us.AddNewUser(user.Name, user.Login, user.Password);

            repositoryMock.Verify(k => k.Insert(user), Times.Once());
        }

        [Test]
        public void CheckLogIn()
        {
            Mock<IUserRepository> repositoryMock = new();
            UserService us = new(repositoryMock.Object);
            User user = UserObjectMother.CreateRegisteredUserWithNameMarkoGolovko();

            us.LogIn(user.Login, user.Password);

            repositoryMock.Verify(k => k.GetUserByEmail(user.Login), Times.Once());
        }

        [Test]
        public void CheckChangeAccountInfo()
        {
            Mock<IUserRepository> repositoryMock = new();
            UserService us = new(repositoryMock.Object);
            User user = UserObjectMother.CreateRegisteredUserWithNameMarkoGolovko();

            us.ChangeAccountInfo(user.Login, x => x.Login = "New Login");

            repositoryMock.Verify(k => k.GetUserByEmail(user.Login), Times.Once());
        }
    }
}

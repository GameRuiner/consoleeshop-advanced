﻿using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.BusinessLogic
{
    public interface IProductService
    {
        IList<Product> GetAllProducts();

        IList<Product> GetProductsByName(string key);

        Product GetProductById(int id);

        Product AddNewProduct(string name, double price);

        Product ChangeProductInfo(int id, Action<Product> action);
    }
}

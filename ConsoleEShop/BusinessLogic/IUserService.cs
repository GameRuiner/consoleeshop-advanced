﻿using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.BusinessLogic
{
    public interface IUserService
    {
        IList<User> GetAllUsers();

        List<User> GetUsersByName(string key);

        User AddNewUser(string name, string email, string password);

        User LogIn(string login, string password);

        User ChangeAccountInfo(string login, Action<User> action);
    }
}

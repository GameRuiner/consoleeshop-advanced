﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.Model
{
    public enum OrderStatus
    {
        New,
        CanceledByAdministrator,
        CanceledByUser,
        PaymentReceived,
        Sent,
        Received,
        Completed
    }
}

﻿using ConsoleEShop.DataAccess;
using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.BusinessLogic
{
    public interface IOrderService
    {
        Order CreateNewOrder(int ItemId, int quantitity, User user, IProductService productService);

        IEnumerable<Order> GetUserOrders(User user);


        bool CancelOrder(int orderId, User user);

        bool RecieveOrder(int orderId, User user);

        Order GetOrderById(int orderId);

        Order ChangeOrderStatus(int orderId, Action<Order> action);
    }
}

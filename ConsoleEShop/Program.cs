﻿using ConsoleEShop.BusinessLogic;
using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop
{
    class Program
    {
        static IProductService productService;
        static IUserService userService;
        static IOrderService orderService;
        static User user;
        static void Main()
        {
            productService = new ProductService();
            userService = new UserService();
            orderService = new OrderService();
            user = null;    
            Console.WriteLine("Welcome to my shop!");
            while (true)
            {
                if (user == null)
                {
                    string mesasge = "Choose option:\n1.View the list of items\n2.Find item by name\n3.Sign up\n4.Log In";
                    Console.WriteLine(mesasge);
                    string answer = Console.ReadLine().Trim();
                    switch (answer)
                    {
                        case "1":
                            ListOfItems();
                            break;
                        case "2":
                            Console.WriteLine("Search phrase:");
                            string searchPhrase = Console.ReadLine().Trim();
                            ListOfItems(searchPhrase);
                            break;
                        case "3":
                            SignUp();
                            break;
                        case "4":
                            LogIn();
                            break;
                        default:
                            Console.WriteLine("Try again");
                            break;
                    }
                } else if (user.IsAdmin == false)
                {
                    string mesasge = "Choose option:\n1.View the list of items\n2.Find item by name";
                    mesasge = "Hi " + user.Name + "!\n" + mesasge;
                    mesasge += "\n3.Create new order\n4.Cancel order\n5.Show orders" +
                               "\n6.Confirm receipt of order\n7.Change personal information" +
                               "\n8.Log out";
                    Console.WriteLine(mesasge);
                    string answer = Console.ReadLine().Trim();
                    switch (answer)
                    {
                        case "1":
                            ListOfItems();
                            break;
                        case "2":
                            Console.WriteLine("Search phrase:");
                            string searchPhrase = Console.ReadLine().Trim();
                            ListOfItems(searchPhrase);
                            break;
                        case "3":
                            CreateOrder();
                            break;
                        case "4":
                            CancelOrder();
                            break;
                        case "5":
                            ShowOrders();
                            break;
                        case "6":
                            RecieveOrder();
                            break;
                        case "7":
                            ChangeAccountData(user.Login);
                            break;
                        case "8":
                            LogOut();
                            break;
                        default:
                            Console.WriteLine("Try again");
                            break;
                    }
                } else
                {
                    string mesasge = "Choose option:\n1.View the list of items\n2.Find item by name";
                    mesasge = "Hi admin " + user.Name + "!\n" + mesasge;
                    mesasge += "\n3.Create new order\n4.Show users" + 
                               "\n5.Add new product\n6.Change product information" +
                               "\n7.Change order status" + "\n8.Log out";
                    Console.WriteLine(mesasge);
                    string answer = Console.ReadLine().Trim();
                    switch (answer)
                    {
                        case "1":
                            ListOfItems();
                            break;
                        case "2":
                            Console.WriteLine("Search phrase:");
                            string searchPhrase = Console.ReadLine().Trim();
                            ListOfItems(searchPhrase);
                            break;
                        case "3":
                            CreateOrder();
                            break;
                        case "4":
                            ShowUsers();
                            break;
                        case "5":
                            AddNewProduct();
                            break;
                        case "6":
                            ChangeProductData();
                            break;
                        case "7":
                            ChangeOrderStatus();
                            break;
                        case "8":
                            LogOut();
                            break;
                        default:
                            Console.WriteLine("Try again");
                            break;
                    }

                }
            }
        }

        static bool ChangeOrderStatus()
        {
            int orderId = -1;
            ValidateInput("Please enter order ID", "ID should be a positive number", x => int.TryParse(x, out orderId) && orderId > 0);
            Console.WriteLine("What status would you like to set?");
            int answer = -1;
            ValidateInput("1.Canceled by administrator\n2.Payment received\n3.Sent\n4.Received\n5.Completed", "Answer should be a number", x => int.TryParse(x, out answer) && answer > 0 && answer < 6);
            Order changedOrder = null;
            switch (answer)
            {
                case 1:
                    changedOrder = orderService.ChangeOrderStatus(orderId, x => x.Status = OrderStatus.CanceledByAdministrator);
                    break;
                case 2:
                    changedOrder = orderService.ChangeOrderStatus(orderId, x => x.Status = OrderStatus.PaymentReceived);
                    break;
                case 3:
                    changedOrder = orderService.ChangeOrderStatus(orderId, x => x.Status = OrderStatus.Sent);
                    break;
                case 4:
                    changedOrder = orderService.ChangeOrderStatus(orderId, x => x.Status = OrderStatus.Received);
                    break;
                case 5:
                    changedOrder = orderService.ChangeOrderStatus(orderId, x => x.Status = OrderStatus.Completed);
                    break;
                default:
                    Console.WriteLine("Try again");
                    break;
            }
            if (changedOrder == null)
                return false;
            return true;
        }

        static bool ChangeProductData()
        {
            int productId = -1;
            ValidateInput("Please enter product ID", "ID should be a positive number", x => int.TryParse(x, out productId) && productId > 0);
            Console.WriteLine("What information you would like to change?");
            int answer = -1;
            Product changedproduct = null;
            ValidateInput("1.Name\n2.Price", "Answer should be a number", x => int.TryParse(x, out answer) && answer > 0 && answer < 4);
            switch (answer)
            {
                case 1:
                    string newName = ValidateInput("New user name:", "Product Name cannot be empty", x => x.Length > 0);
                    changedproduct = productService.ChangeProductInfo(productId, x => x.Name = newName);
                    break;
                case 2:
                    double newPrice = -1;
                    ValidateInput("Price:", "Price should be a number", x => double.TryParse(x, out newPrice) && newPrice > 0);
                    changedproduct = productService.ChangeProductInfo(productId, x => x.Price = newPrice);
                    break;
                default:
                    Console.WriteLine("Try again");
                    break;
            }
            if (changedproduct == null)
                return false;
            return true;
        }

        static void AddNewProduct()
        {
            double price = -1;
            string prouctName = ValidateInput("Product name:", "Name cannot be empty", x => x.Length > 0);
            ValidateInput("Price:", "Price should be a number", x => double.TryParse(x, out price) && price > 0);
            Product product = productService.AddNewProduct(prouctName, price);
            if (product != null)
            {
                Console.WriteLine($"Product no {product.Id} {product.Name} with price {product.Price} created successfully");
            }
            else
            {
                Console.WriteLine("Product wasn't added");
            }
        }

        static void ShowUsers()
        {
            var users = userService.GetAllUsers();
            foreach (var user in users)
            {
                Console.WriteLine($"Login: {user.Login}, Name: {user.Name}{(user.IsAdmin ? ", user is admin" : "")}.");
            }
            Console.WriteLine("Whould you like to change account info?");
            int answer = -1;
            ValidateInput("1.Yes\n2.No", "Answer should be a number", x => int.TryParse(x, out answer) && answer > 0 && answer < 3);
            switch (answer)
            {
                case 1:
                    Console.WriteLine("Please enter account login:");
                    string login = ValidateInput("Login:", "Login cannot be empty", x => x.Length > 0);
                    var res = ChangeAccountData(login);
                    if (res)
                    {
                        Console.WriteLine("Account changed successfuly");
                    } else
                    {
                        Console.WriteLine("Error in changing account");
                    }
                    break;
                default:
                    break;
            }
        }

        static void LogOut()
        {
            user = null;
        }

        static bool ChangeAccountData(string login)
        {
            Console.WriteLine("What information you would like to change?");
            int answer = -1;
            User changedUser = null;
            ValidateInput("1.Name\n2.Login\n3.Password:", "Answer should be a number", x => int.TryParse(x, out answer) && answer > 0 && answer < 4);
            switch (answer)
                {
                    case 1:
                        string newName = ValidateInput("New user name:", "User Name cannot be empty", x => x.Length > 0);
                        changedUser = userService.ChangeAccountInfo(login, x => x.Name = newName);
                        break;
                    case 2:
                        string newLogin = ValidateInput("New login:", "Login cannot be empty", x => x.Length > 0);
                        changedUser = userService.ChangeAccountInfo(login, x => x.Login = newLogin);
                    break;
                    case 3:
                        string newPassword = ValidateInput("New password:", "Password must be 8 to 20 characters", x => x.Length > 7 && x.Length < 21);
                        changedUser = userService.ChangeAccountInfo(login, x => x.Password = newPassword);
                        break;
                    default:
                        Console.WriteLine("Try again");
                        break;
            }
            if (changedUser == null)
                return false;
            return true;
        }

        static void RecieveOrder()
        {
            int orderId = -1;
            ValidateInput("Order Id:", "Id should be a number", x => int.TryParse(x, out orderId) && orderId > 0);
            var result = orderService.RecieveOrder(orderId, user);
            if (result)
            {
                Console.WriteLine(string.Format("Order no {0} changed status to recieved", orderId));
            }
            else
            {
                Console.WriteLine(string.Format("Order no {0} wasn't recieved", orderId));
            }
        }

        static void ShowOrders()
        {
            var orders = orderService.GetUserOrders(user).ToList();
            if (orders.Count == 0)
            {
                Console.WriteLine("You have no orders");
            }
            else
            {
                Console.WriteLine("Your orders:");
            }
            foreach (var order in orders)
            {
                Console.WriteLine("No {0}, Created {1}, Status {2}", order.Id, order.DateTime, order.Status);
            }
        }

        static void CancelOrder()
        {
            int orderId = -1;
            ValidateInput("Order Id:", "Id should be a number", x => int.TryParse(x, out orderId) && orderId > 0);
            var result = orderService.CancelOrder(orderId, user);
            if (result)
            {
                Console.WriteLine(string.Format("Order no {0} successfully cancelled", orderId));
            } else
            {
                Console.WriteLine(string.Format("Order no {0} wasn't cancelled", orderId));
            }
        }

        static void CreateOrder()
        {
            int itemId = -1;
            int quantitity = -1;
            ValidateInput("Item ID:", "ID should be a number", x => int.TryParse(x, out itemId) && itemId > 0);
            ValidateInput("Quantitity:", "Quantitity should be a number", x => int.TryParse(x, out quantitity) && quantitity > 0);
            Order order = orderService.CreateNewOrder(itemId, quantitity, user, productService);
            if (order != null)
            {
                Console.WriteLine(string.Format("Order no {0} on {1} created successfully", order.Id, order.OrderItems[0].Product.Name));
            }
            else
            {
                Console.WriteLine("Order wasn't created");
            }  
        }

        static void ListOfItems(string key = null)
        {
            IList<Product> products;
            if (key == null)
            {
                products = productService.GetAllProducts();
            } else
            {
                
                products = productService.GetProductsByName(key);
            }

            foreach (var product in products)
            {
                string message = string.Format("ID: {0}, Name: {1}. Price: {2}$", product.Id, product.Name, product.Price);
                Console.WriteLine(message);
            }
        }

        static void SignUp()
        {
            Console.WriteLine("Let's set up your account");
            string name = ValidateInput("User Name:", "User Name cannot be empty", x => x.Length>0);
            string email = ValidateInput("Email:", "Email cannot be empty", x => x.Length > 0);
            string password = ValidateInput("Password:", "Password must be 8 to 20 characters", x => x.Length > 7 && x.Length < 21);
            var result = userService.AddNewUser(name, email, password);
            if (result == null)
            {
                Console.WriteLine("User with this login already exists");
            } else
            {
                Console.WriteLine("Account successfully created");
            }
        }

        static void LogIn()
        {
            Console.WriteLine("Log into Your Account");
            string email = ValidateInput("Email:", "User Name cannot be empty", x => x.Length > 0);
            string password = ValidateInput("Password:", "Password canot be empty", x => x.Length > 0);
            user = userService.LogIn(email, password);
        }

        static string ValidateInput(String message, String errorMessage, Predicate<String> predicate)
        {
            Console.WriteLine(message);
            String name = Console.ReadLine().Trim();
            while (!predicate(name))
            {
                Console.WriteLine(errorMessage);
                name = Console.ReadLine().Trim();
            }
            return name;
        }
    }
}

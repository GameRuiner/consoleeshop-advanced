﻿using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.DataAccess
{
    public interface IProductRepository
    {
        List<Product> FindAll();

        List<Product> FindByName(string key);

        Product FindById(int id);

        int GetNewID();

        void Insert(Product product);
    }
}

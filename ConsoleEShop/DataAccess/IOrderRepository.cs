﻿using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.DataAccess
{
    public interface IOrderRepository
    {
        int GetNewID();

        void Insert(Order order);

        Order FindById(int id);

        IEnumerable<Order> FindByUser(User user);
    }
}

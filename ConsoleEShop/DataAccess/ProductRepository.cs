﻿using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.DataAccess
{
    class ProductRepository : IProductRepository
    {
        readonly List<Product> products = new();

        public ProductRepository()
        {
            products = new List<Product>
            {
                new Product() { Id = 1, Name = "Pop It", Price = 5},
                new Product() { Id = 2, Name = "Simple dimple", Price = 6},
                new Product() { Id = 3, Name = "Popcorn", Price = 2},
            };
        }

        public List<Product> FindAll()
        {
            return products;
        }

        public List<Product> FindByName(string key)
        {
            return (from product in products
                   where product.Name.ToLower().Contains(key.Trim().ToLower())
                   select product).ToList();
        }

        public Product FindById(int id)
        {
            return (from product in products
                    where product.Id == id
                    select product).FirstOrDefault();
        }

        public int GetNewID()
        {
            return products.Max(o => o.Id) + 1;
        }

        public void Insert(Product product)
        {
            products.Add(product);
        }
    }
}

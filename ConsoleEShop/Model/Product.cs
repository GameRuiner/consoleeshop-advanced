﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.Model
{
    public class Product
    {
        public int Id;
        public string Name;
        public double Price;
    }
}

﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleEShop.DataAccess;
using ConsoleEShop.BusinessLogic;
using ConsoleEShop.Model;
using ObjectMothers;

namespace TestEShop
{
    class OrderServiceTest
    {
        [Test]
        public void CheckCreateNewOrder()
        {
            Mock<IOrderRepository> repositoryMock = new();
            OrderService os = new(repositoryMock.Object);
            User user = UserObjectMother.CreateRegisteredUserWithNameMarkoGolovko();
            Mock<IProductRepository> mock = new();
            ProductService ps = new(mock.Object);

            int id = 1;
            int quantitity = 1;
            Order order = os.CreateNewOrder(id, quantitity, user, ps);

            mock.Verify(k => k.FindById(id), Times.Once);
        }

        [Test]
        public void CheckGetOrderById()
        {
            Mock<IOrderRepository> repositoryMock = new();
            OrderService os = new(repositoryMock.Object);

            int id = 1;
            os.GetOrderById(id);

            repositoryMock.Verify(k => k.FindById(id), Times.Once());
        }

        [Test]
        public void CheckCancelOrder()
        {
            Mock<IOrderRepository> repositoryMock = new();
            OrderService os = new(repositoryMock.Object);
            User user = UserObjectMother.CreateRegisteredUserWithNameMarkoGolovko();

            int id = 1;
            os.CancelOrder(id, user);

            repositoryMock.Verify(k => k.FindById(id), Times.Once());
        }

        [Test]
        public void CheckRecieveOrder()
        {
            Mock<IOrderRepository> repositoryMock = new();
            OrderService os = new(repositoryMock.Object);
            User user = UserObjectMother.CreateRegisteredUserWithNameMarkoGolovko();

            int id = 1;
            os.RecieveOrder(id, user);

            repositoryMock.Verify(k => k.FindById(id), Times.Once());
        }

        [Test]
        public void CheckGetUsersOrder()
        {
            Mock<IOrderRepository> repositoryMock = new();
            OrderService os = new(repositoryMock.Object);
            User user = UserObjectMother.CreateRegisteredUserWithNameMarkoGolovko();

            os.GetUserOrders(user);

            repositoryMock.Verify(k => k.FindByUser(user), Times.Once());
        }

        [Test]
        public void CheckChangeOrderStatus()
        {
            Mock<IOrderRepository> repositoryMock = new();
            OrderService os = new(repositoryMock.Object);
            User user = UserObjectMother.CreateRegisteredUserWithNameMarkoGolovko();

            int id = 1;
            os.ChangeOrderStatus(id, x => x.Status = OrderStatus.PaymentReceived);

            repositoryMock.Verify(k => k.FindById(id), Times.Once());
        }

    }
}

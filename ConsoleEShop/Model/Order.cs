﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.Model
{
    public class Order
    {
        public int Id { get; set; }
        public User Customer { get; set; }
        public DateTime DateTime { get; set; }
        public OrderStatus Status { get; set; }
        public List<OrderItems> OrderItems { get; set; }
    }
}

﻿using ConsoleEShop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEShop.DataAccess
{
    public interface IUserRepository
    {
        public List<User> FindAll();

        public void Insert(User user);

        public List<User> FindByName(string key);

        public User GetUserByEmail(string key);
    }
}
